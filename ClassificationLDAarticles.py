#Classification Non supervisé par LDA d'articles de presse

import pandas as pd

#Utilisation du dataset National Public Radio 
npr = pd.read_csv('npr.csv')

npr.head()

#pre-process
from sklearn.feature_extraction.text import CountVectorizer

cv = CountVectorizer(max_df=0.95, min_df=2, stop_words='english')
dtm = cv.fit_transform(npr['Article'])


#LDA

from sklearn.decomposition import LatentDirichletAllocation
LDA = LatentDirichletAllocation(n_components=7,random_state=42)
LDA.fit(dtm)

#affichage des topics et des 15 top words pour le topic

for index,topic in enumerate(LDA.components_):
    print(f'THE TOP 15 WORDS FOR TOPIC #{index}')
    print([cv.get_feature_names()[i] for i in topic.argsort()[-15:]])
    print('\n')

#Affichage des articles du Df avec le topic associé
topic_results = LDA.transform(dtm)
topic_results.argmax(axis=1)
npr['Topic'] = topic_results.argmax(axis=1)

npr.head(10)






